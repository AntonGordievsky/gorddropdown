## gordDropdown ##

This component creates a customizable dropdown element.

[Demo](https://gordievskiy.com/lab/gordDropdown)

## How do I get set up? ##

**First, make sure you are using valid DOCTYPE and the jQuery library.**

**Include necessary JS file:**

```
#!html

<script src="js/gordDropdown.js" type="text/javascript"></script>
```

**Create an HTML structure:**

```
#!html

<div class="select"></div>
```

The component itself will create its own internal structure like this:

```
#!html

<div class="select-container">
    <div class="select" onselectstart="return false">Item 1</div>
    <ul class="drop">
        <li>Item 1</li>
        <li>Item 2</li>
        <li>Item 3</li>
        <li>Item 4</li>
    </ul>
</div>
```

*The active <li> element have a class "active". So you can customize it.*

**Fire plugin using jQuery selector**

This is a basic initialization (uses default options):

```
#!javascript

$(selector).gordDropdown();
```

Or you can set a custom options:

```
#!javascript
$('.select').gordDropdown({
    items: [
        'Item 1',
        'Item 2',
        'Item 3',
        'Item 4'
    ],
    itemsValues: [
        1,
        2,
        3,
        4
    ],

    // Optional settings

    // Default: 150 ms
    speedDown    : 200,

    // Default: 150 ms
    speedUp      : 100
});
```

By using the jQuery method *data()*, the items values are bound to each drop-down list item.

Track the select event and get the data you can use this code:

```
#!javascript

$(document).on('select', function (event, value) {
    console.log(value);
});
```

*You can initialize instances of the effect one at a time, or you can specify settings for multiple instances at once, if they are the same.*