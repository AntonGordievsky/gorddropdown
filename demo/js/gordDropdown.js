/**
 * @author   Antonio Gordievskiy
 * @module   jQuery module
 * @name     gordDropdown
 * @link     https://bitbucket.org/AntonGordievsky/gorddropdown
 * @throws   If the containers for component is not specified,
 *           an exception is {"Container is not defined"}
 * @throws   If the items to select is not defined,
             an exception is {"No items to select"}
 */

(function ( $ ) {

    $.fn.gordDropdown = function ( options ) {
        var settings = $.extend({
            containers   : $( this ),
            items        : [],
            itemsValues  : [],
            speedDown    : 150,
            speedUp      : 150
        }, options );

        if ( settings.containers.length == 0 ) {
            console.warn( 'Container is not defined' );
        } else if ( settings.items.length == 0 ) {
            console.warn( 'No items to select' );
        } else if ( settings.itemsValues.length == 0 ) {
            console.warn( 'No items values to select' );
        }

        function createContent (container) {
            var frag = document.createDocumentFragment();

            var select = $(document.createElement('div')).addClass('select')
                .attr('onselectstart', 'return false;').html(settings.items[0]);
            var drop = $(document.createElement('ul')).addClass('drop');

            $( settings.items ).each( function ( i ) {
                $(drop).append(
                    $(document.createElement( 'li' )).text( settings.items[i] )
                        .data( 'value', settings.itemsValues[i] )
                );
            });

            $(frag).append(select, drop);
            $(container).append(frag);
        }

        $(settings.containers).each( function ( i ) {
            $(this).addClass('select-container');

            createContent( $(this) );

            var select       = $(this).find('.select'),
                dropDownList = $(this).find('.drop'),
                selectedItem = $(dropDownList).children().eq(0).addClass('active');

            $(select).click( function () {
                if( dropDownList.is(':hidden') ) {
                    dropDownList.slideDown(settings.speedDown);
                $(select).addClass('active');
                } else {
                    $(select).removeClass('active');
                    dropDownList.slideUp(settings.speedUp);
                }
            });

            $(dropDownList).children().on( 'click', function ( event ) {
                if ( selectedItem[0] == event.target ) {
                    dropDownList.slideUp(settings.speedUp);
                    $(select).removeClass('active');
                    return;
                };

                $(select).html($(event.target).html());
                $(select).removeClass('active');
                dropDownList.slideUp(settings.speedUp);
                $(selectedItem).removeClass('active');
                $(this).addClass('active');
                selectedItem = $(this);

                $(event.target).trigger('select', [
                    $(this).data('value')
                ]);
            });
        });

        return this;
    }

})( jQuery );
